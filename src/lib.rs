use std::time::SystemTime;
use std::net::{UdpSocket, SocketAddr, ToSocketAddrs};
use std::mem::size_of;

/// The top-level message struct. The size is equivalent to the largest possible
/// message.
#[derive(Debug, Clone, Copy, PartialEq)]
#[repr(C)]
pub struct Message
{
    // Message header
    pub length: u16,
    pub time1: SystemTime,
    pub time2: SystemTime,

    // Message body, which differs per message
    pub payload: MessagePayload
}

/// Enumeration of all possible message types. Each variant contains each field
/// associated with the corresponding message type.
#[derive(Debug, Clone, Copy, PartialEq)]
#[repr(C)]
pub enum MessagePayload
{
    Null,
    Message1
    {
        field1: u32,
        field2: i64,
    },
    Message2
    {
        field1: u8,
        field2: u8,
        field3: [u8; 10]
    },
    Message3
}

impl MessagePayload
{
    /// Get the payload size
    pub const fn size_of_payload(&self) -> usize
    {
        match self
        {
            Self::Null => 8,
            Self::Message1 { .. } => 24,
            Self::Message2 { .. } => 24,
            Self::Message3 => 8,
        }
    }

    /// Calculate the total size of the message containing this payload. 
    pub const fn size_with_header(&self) -> usize
    {
        (size_of::<Message>() - size_of::<MessagePayload>()) + self.size_of_payload()
    }
}

/// A union that allows a message struct to be interpreted as raw bytes
#[repr(C)]
pub union MessageBuffer
{
    pub bytes: [u8; size_of::<Message>()],
    pub message: Message
}

impl MessageBuffer
{
    /// Create a blank message buffer
    pub fn new() -> Self
    {
        MessageBuffer 
        {
            bytes: [0; size_of::<Message>()]
        }
    }

    /// Send a message over a UDP socket
    pub fn send(&self, socket: &UdpSocket, addr: impl ToSocketAddrs) -> std::io::Result<usize>
    {
        unsafe 
        {
            socket.send_to(&self.bytes[0..self.message.payload.size_with_header()], addr)
        }
    }

    /// Receive a message  from the given UDP socket
    pub fn receive(&mut self, socket: &UdpSocket) -> std::io::Result<(usize, SocketAddr)>
    {
        unsafe 
        {
           socket.recv_from(&mut self.bytes)
        }
    }

    /// Create a shared reference to the inner message
    pub fn message(&self) -> &Message
    {
        unsafe 
        {
           &self.message
        }
    }

    /// Create a mutable reference to the inner message
    pub fn message_mut(&mut self) -> &mut Message
    {
        unsafe 
        {
           &mut self.message
        }
    }
}