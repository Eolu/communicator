#![allow(dead_code)]

mod lib;
#[cfg(test)]
mod tests;

use std::{net::UdpSocket};
use lib::*;

fn main() -> std::io::Result<()>
{
    let mut message_buffer = MessageBuffer::new();
    let socket = UdpSocket::bind("0.0.0.0:34251")?;
    loop
    {
        let (_number_of_bytes, _src_addr) = message_buffer.receive(&socket)?;
        message_buffer.message_mut().time2 = std::time::SystemTime::now();
        let message = message_buffer.message();

        // Compare timestamps
        match message.time2.duration_since(message.time1)
        {
            Ok(stc_time) => println!("{stc_time:?}"),
            Err(error) => eprintln!("{error:?}")
        }
    }
}
