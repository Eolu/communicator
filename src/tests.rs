use std::time::SystemTime;
use crate::lib::*;

fn validate_message_size(payload: MessagePayload)
{
    // Create a message buffer
    let mut message_buffer = MessageBuffer::new();

    // Create a full message containing the payload
    let message = Message
    {
        length: payload.size_with_header() as u16,
        time1: SystemTime::now(),
        time2: SystemTime::now(),
        payload: payload
    };

    message_buffer.message = message;

    // Copy message buffer raw bytes into another
    let mut message_buffer_2 = MessageBuffer::new();
    unsafe 
    {
        for i in 0..message_buffer.message.payload.size_with_header()
        {
            message_buffer_2.bytes[i] = message_buffer.bytes[i];
        }
        message_buffer.message = message;
    }

    // Convert back to message and compare buffers
    unsafe 
    {
        assert_eq!(message_buffer.message, message_buffer_2.message);
    }
}

#[test]
fn validate_message_1_size()
{
    let message_1 = MessagePayload::Message1 
    { 
        field1: 22, 
        field2: 34 
    };

    validate_message_size(message_1);
}

#[test]
fn validate_message_2_size()
{
    let message_2 = MessagePayload::Message2 
    { 
        field1: 4, 
        field2: 5, 
        field3: [0x68,0x65,0x6C,0x6C,0x6F,0x20,0x79,0x6F,0x75,0x21]
    };

    validate_message_size(message_2);
}

#[test]
fn validate_message_3_size()
{
    validate_message_size(MessagePayload::Message3);
}