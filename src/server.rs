#![allow(dead_code)]

mod lib;
#[cfg(test)]
mod tests;

use std::net::UdpSocket;
use std::time::{SystemTime, Duration, Instant};
use std::thread::sleep;
use lib::*;

fn main() -> std::io::Result<()>
{
    let mut message_buffer = MessageBuffer::new();
    let mut dummy_message_1 = Message
    {
        length: 16,
        time1: SystemTime::now(),
        time2: SystemTime::now(),
        payload: MessagePayload::Message1 
        { 
            field1: 22, 
            field2: 34 
        }
    };
    let mut dummy_message_2 = Message
    {
        length: 24,
        time1: SystemTime::now(),
        time2: SystemTime::now(),
        payload: MessagePayload::Message2 
        { 
            field1: 4, 
            field2: 5, 
            field3: [0x68,0x65,0x6C,0x6C,0x6F,0x20,0x79,0x6F,0x75,0x21]
        }
    };
    let mut dummy_message_3 = Message
    {
        length: 8,
        time1: SystemTime::now(),
        time2: SystemTime::now(),
        payload: MessagePayload::Message3
    };

    let socket = UdpSocket::bind("0.0.0.0:34254")?;
    loop
    {
        let start_time = Instant::now();

        dummy_message_1.time1 = SystemTime::now();
        message_buffer.message = dummy_message_1;
        message_buffer.send(&socket, "0.0.0.0:34251")?;

        dummy_message_2.time1 = SystemTime::now();
        message_buffer.message = dummy_message_2;
        message_buffer.send(&socket, "0.0.0.0:34251")?;

        dummy_message_3.time1 = SystemTime::now();
        message_buffer.message = dummy_message_3;
        message_buffer.send(&socket, "0.0.0.0:34251")?;

        // Run at 200hz
        match Instant::now().checked_duration_since(start_time)
        {
            Some(duration) => 
            {
                sleep(Duration::from_millis(5) - duration)
            }
            None => 
            {
                eprintln!("Monotonoic clock failure")
            }
        }
    }
}

